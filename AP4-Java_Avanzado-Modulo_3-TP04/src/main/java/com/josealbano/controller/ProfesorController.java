package com.josealbano.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.josealbano.model.Profesor;
import com.josealbano.service.IProfesorService;

@Controller
@RequestMapping("/app")
public class ProfesorController {
  @Autowired
  private IProfesorService profesorService;

  @RequestMapping("/profesores")
  public String mostrarProfesores(Model modelo) {
    List<Profesor> teachers = profesorService.getAll();
    
    modelo.addAttribute("listaProfesores", teachers);
    
    return "profesores";
  }
}