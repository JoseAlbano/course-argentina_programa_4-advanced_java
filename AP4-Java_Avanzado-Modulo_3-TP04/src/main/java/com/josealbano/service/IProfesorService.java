package com.josealbano.service;

import java.util.List;

import com.josealbano.model.Profesor;

public interface IProfesorService {
  void create(Profesor teacher);
  List<Profesor> getAll();
  Profesor getById(Long id);
  void update(Profesor teacher);
  void delete(Long id);
}