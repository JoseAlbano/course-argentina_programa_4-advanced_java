# My Learning Path: Java Software Developer

## Course: *Java Programming: Advanced Level* by *[Universidad Nacional de Jujuy (UNJu)](https://unju.edu.ar/)*

### Learning Path of *[Argentina Programa 4.0](https://www.argentina.gob.ar/economia/conocimiento/argentina-programa)*

Hello.

Here are my Solutions to the Exercises in the Course.

### My Progress

- [x] Module 01: Introduction to Spring Boot.
- [x] Module 02: API's with Spring Boot.
- [x] Module 03: CRUD with JPA + Hibernate.
