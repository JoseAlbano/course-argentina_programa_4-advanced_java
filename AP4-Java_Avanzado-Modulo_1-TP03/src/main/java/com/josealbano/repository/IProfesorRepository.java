package com.josealbano.repository;

import org.springframework.data.repository.CrudRepository;

import com.josealbano.model.Profesor;

public interface IProfesorRepository extends CrudRepository<Profesor, Long> {

}