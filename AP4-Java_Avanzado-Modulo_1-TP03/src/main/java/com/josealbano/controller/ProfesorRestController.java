package com.josealbano.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.josealbano.model.Profesor;
import com.josealbano.service.IProfesorService;

@RestController
@RequestMapping("/api/profesores")
public class ProfesorRestController {
  
  @Autowired
  private IProfesorService profesorService;
  
  @PostMapping
  public ResponseEntity<Profesor> create(@RequestBody Profesor teacher) {
    profesorService.create(teacher);    
    return ResponseEntity.status(HttpStatus.CREATED).body(teacher);
  }

  @GetMapping
  public List<Profesor> getAllProfesores() {
    return profesorService.getAll();
  }
  
  @GetMapping("/{id}")
  public Profesor getProfesorById(@PathVariable long id) {
    return profesorService.getById(id);
  }
  
  @PutMapping("/{id}")
  public ResponseEntity<Profesor> update(@PathVariable long id, @RequestBody Profesor teacher) {
    profesorService.update(teacher); 
    return ResponseEntity.status(HttpStatus.CREATED).body(teacher);
  }
  
  @DeleteMapping("/{id}")
  public ResponseEntity<Profesor> delete(@PathVariable long id) {
    Profesor teacherToDelete = profesorService.getById(id);
    profesorService.delete(id);
    return ResponseEntity.status(HttpStatus.OK).body(teacherToDelete);
  }
}