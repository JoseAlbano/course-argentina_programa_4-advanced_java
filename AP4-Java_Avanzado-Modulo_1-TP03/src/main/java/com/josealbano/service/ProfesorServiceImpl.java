package com.josealbano.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.josealbano.model.Profesor;
import com.josealbano.repository.IProfesorRepository;

@Service
public class ProfesorServiceImpl implements IProfesorService {
  @Autowired
  IProfesorRepository profesorRepository;
  
  @Override
  public void create(Profesor teacher) {
    profesorRepository.save(teacher);
  }

  @Override
  public List<Profesor> getAll() {
    return (List<Profesor>) profesorRepository.findAll();
  }
  
  @Override
  public Profesor getById(Long id) {  
    return profesorRepository.findById(id).get();    
  }

  @Override
  public void update(Profesor teacher) {
    profesorRepository.save(teacher);
  }

  @Override
  public void delete(Long id) {
    profesorRepository.deleteById(id);
  }
}