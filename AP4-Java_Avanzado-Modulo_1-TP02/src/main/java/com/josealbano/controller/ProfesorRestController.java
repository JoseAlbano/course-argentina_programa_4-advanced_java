package com.josealbano.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.josealbano.model.Profesor;
import com.josealbano.service.IProfesorService;

@RestController
@RequestMapping("/api/profesores")
public class ProfesorRestController {
  @Autowired
  IProfesorService profesorService;

  public ProfesorRestController(IProfesorService profesorService) {
    this.profesorService = profesorService;
  }

  @PostMapping("/create")
  public ResponseEntity<Profesor> createProfesor(@RequestBody Profesor teacher) {
    profesorService.create(teacher);

    return ResponseEntity.status(HttpStatus.CREATED).body(teacher);
  }

  @GetMapping
  public List<Profesor> getAllProfesores() {
    return profesorService.getAll();
  }

  @GetMapping("/get/{id}")
  public Profesor getProfesorById(@PathVariable int id) {
    return profesorService.getById(id);
  }
  
  @PutMapping("/update/{id}")
  public ResponseEntity<Profesor> updateProfesor(@PathVariable int id, 
      @RequestBody Profesor teacher) {
    profesorService.update(id, teacher);
    
    return ResponseEntity.status(HttpStatus.OK).body(teacher);
  }
  
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<Profesor> deleteProfesor(@PathVariable int id) {
    Profesor teacher = profesorService.getById(id);
    profesorService.delete(id);
    
    return ResponseEntity.status(HttpStatus.OK).body(teacher);
  }

}