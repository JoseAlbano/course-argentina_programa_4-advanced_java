package com.josealbano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ap4JavaAvanzadoModulo1Tp02Application {

	public static void main(String[] args) {
		SpringApplication.run(Ap4JavaAvanzadoModulo1Tp02Application.class, args);
	}

}
