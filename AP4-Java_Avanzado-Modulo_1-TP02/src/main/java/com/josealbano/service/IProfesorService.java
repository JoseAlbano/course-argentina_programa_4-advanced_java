package com.josealbano.service;

import java.util.List;

import com.josealbano.model.Profesor;

public interface IProfesorService {
  void create(Profesor profesor);
  List<Profesor> getAll();
  Profesor getById(int id);
  void update(int id, Profesor profesor);
  void delete(int id);
}