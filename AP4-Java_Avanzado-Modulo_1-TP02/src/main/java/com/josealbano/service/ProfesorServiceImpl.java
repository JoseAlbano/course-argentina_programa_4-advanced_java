package com.josealbano.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.josealbano.model.Profesor;

@Service
public class ProfesorServiceImpl implements IProfesorService {
  private List<Profesor> teachersList = new ArrayList<>(
      Arrays.asList(
          new Profesor(1, "Albano", "José", "27123456"), 
          new Profesor(2, "Jirafales", "Rubén", "6789456"), 
          new Profesor(3, "Skinner", "Seymour", "10456123")
          )
      );

  @Override
  public void create(Profesor teacher) {
    teachersList.add(teacher);
  }

  @Override
  public List<Profesor> getAll() {
    return teachersList;
  }

  @Override
  public Profesor getById(int id) {
    for (Profesor teacher : teachersList) {
      if (teacher.getId() == id) {
        return teacher;
      }
    }

    return null;
  }

  @Override
  public void update(int id, Profesor updatedTeacher) {
    
    for (Profesor teacher : teachersList) {
    
      if (teacher.getId() == id) {

        teacher.setApellidos( updatedTeacher.getApellidos() );
        teacher.setNombres( updatedTeacher.getNombres() );
        teacher.setLegajo( updatedTeacher.getLegajo() );
      }
    }
  }

  @Override
  public void delete(int id) {
    
    for (Profesor teacher : teachersList) {
      if (teacher.getId() == id) {
        teachersList.remove(teacher);
        break;
      }
    }
  }
}
