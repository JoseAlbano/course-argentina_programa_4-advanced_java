package com.josealbano.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Profesor {
  private int id;
  private String apellidos;
  private String nombres;
  private String legajo;
  
}