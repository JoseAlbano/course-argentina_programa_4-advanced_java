package com.josealbano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ap4JavaAvanzadoTpFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ap4JavaAvanzadoTpFinalApplication.class, args);
	}

}
