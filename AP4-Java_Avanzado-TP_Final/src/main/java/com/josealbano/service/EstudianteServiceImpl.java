/**
 * 
 */
package com.josealbano.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.josealbano.dao.IEstudianteDao;
import com.josealbano.model.Estudiante;

/**
 * @author José Albano
 */
@Service
public class EstudianteServiceImpl implements IEstudianteService {
  @Autowired
  IEstudianteDao estudianteDao;

  @Override
  public void create(Estudiante student) {
    estudianteDao.save(student);
  }

  @Override
  public List<Estudiante> getAll() {
    return (List<Estudiante>) estudianteDao.findAll();
  }

  @Override
  public Estudiante getById(Long id) {
    return estudianteDao.findById(id).get();
  }

  @Override
  public void update(Estudiante student) {
    estudianteDao.save(student);
  }

  @Override
  public void delete(Long id) {
    estudianteDao.deleteById(id);
  }

}
