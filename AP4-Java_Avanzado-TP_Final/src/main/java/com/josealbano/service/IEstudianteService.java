package com.josealbano.service;

import java.util.List;

import com.josealbano.model.Estudiante;

/**
 * @author José Albano
 */
public interface IEstudianteService {
  void create(Estudiante student);
  List<Estudiante> getAll();
  Estudiante getById(Long id);
  void update(Estudiante student);
  void delete(Long id);
}