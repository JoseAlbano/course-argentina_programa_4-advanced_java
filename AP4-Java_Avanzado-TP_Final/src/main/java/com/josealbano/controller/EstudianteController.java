package com.josealbano.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.josealbano.model.Estudiante;
import com.josealbano.service.IEstudianteService;

/**
 * @author José Albano
 */
@Controller
@RequestMapping("/estudiantes")
public class EstudianteController {
  @Autowired
  private IEstudianteService estudianteService;
  
  //@GetMapping("/list")
  @GetMapping
  public String mostrarEstudiantes(Model model) {
    model.addAttribute("listaEstudiantes", estudianteService.getAll());
    
    return "estudiantes";
  }
  
  @GetMapping("/new")
  public String mostrarFormularioCreacion(Model model) {
    Estudiante estudiante = new Estudiante();
    model.addAttribute("estudiante", estudiante);
    return "estudiante_new";
  }
  
  @GetMapping("/edit/{id}")
  public String mostrarFormularioEdicion(@PathVariable long id, Model model) {
    model.addAttribute("estudiante", estudianteService.getById(id)); 
        
    return "estudiante_edit";
  }
  
  @PostMapping("/create")
  public String crearEstudiante(Estudiante estudiante) {
    estudianteService.create(estudiante);
    //return "redirect:list";
    return "redirect:/estudiantes";
  }
  
  @PostMapping("/update/{id}")
  public String guardarEstudiante(@PathVariable long id, 
      @ModelAttribute("estudiante") Estudiante estudiante, Model model) {
    
    Estudiante estudianteExistente = estudianteService.getById(id);
    
    estudianteExistente.setApellido(estudiante.getApellido());
    estudianteExistente.setNombre(estudiante.getNombre());
    estudianteExistente.setEmail(estudiante.getEmail());
    
    estudianteService.update(estudianteExistente);
    
    return "redirect:/estudiantes";
  }
  
  @GetMapping("/delete/{id}")
  public String deleteEstudiante(@PathVariable long id) {
    estudianteService.delete(id);
    return "redirect:/estudiantes";
  }
}