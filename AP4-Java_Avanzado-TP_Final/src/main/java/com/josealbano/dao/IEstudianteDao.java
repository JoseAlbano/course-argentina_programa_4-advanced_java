package com.josealbano.dao;

import org.springframework.data.repository.CrudRepository;

import com.josealbano.model.Estudiante;

public interface IEstudianteDao extends CrudRepository<Estudiante, Long> {

}