package com.josealbano.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.josealbano.model.Profesor;

@Controller
public class ProfesorController {

  @RequestMapping("/profesor")
  public String mostrarProfesor(Model modelo) {
    Profesor teacher = new Profesor("Albano", "José", "27123456");
    
    modelo.addAttribute("unProfesor", teacher);
    return "profesor";
  }
  
  @RequestMapping("/profesores")
  public String mostrarProfesores(Model modelo) {
    List<Profesor> teachers = new ArrayList<>();
    
    teachers.add(new Profesor("Albano", "José", "27123456"));
    teachers.add(new Profesor("Jirafales", "Rubén", "6789456"));
    teachers.add(new Profesor("Skinner", "Seymour", "10456123"));
    
    modelo.addAttribute("listaProfesores", teachers);
    
    return "profesores";
  }
}